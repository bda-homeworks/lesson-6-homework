import java.awt.*;

public class Main {
    public static void main(String[] args) {
        Circle c1 = new Circle( new Point(1, 2), 3);
//        c1.center = new Point(0, 0);
//        c1.radius = 3;
//
//        c1.setCenter(new Point(1, 2));
//        c1.setRadius(5);

        System.out.println("Area = " + c1.getArea());
        System.out.println("Perimeter = " + c1.getPerimeter());
    }
}