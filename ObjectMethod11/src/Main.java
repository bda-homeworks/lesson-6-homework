public class Main {
    public static void main(String[] args) {
        Human adam = new Human();
        Animal heyvan = new Animal();

        heyvan.cixmaq();
        heyvan.tullanmaq();
        heyvan.ucmaq();

        adam.yatmaq();
        adam.eat();
        adam.qacmaq();
    }
}