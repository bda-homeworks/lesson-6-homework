public class Employee {

    String name;
    String surname;
    String department;
    String position;
    String salary;

    public Employee(String name, String surname, String department, String position, String salary) {
        this.name = name;
        this.surname = surname;
        this.department = department;
        this.position = position;
        this.salary = salary;
    }
    public void displayAllInfo () {
        System.out.println("Name: " + this.name);
        System.out.println("Surname: " + this.surname);
        System.out.println("Department: " + this.department);
        System.out.println("Position: " + this.position);
        System.out.println("Salary: " + this.salary);
    }
    public void changeSalary(String money) {
        this.salary = money;
        System.out.println("Changed salary: " + this.salary);
    }
}
