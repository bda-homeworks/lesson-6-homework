import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Employee1 isci = new Employee1(input.next(), input.next(), input.next(), input.next(), input.next());
        isci.displayAllInfo();

        String money = input.next();
        isci.changeSalary(money);

        isci.displayAllInfo();
    }
}