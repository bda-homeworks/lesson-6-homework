public class Main {
    public static void main(String[] args) {
        Class user1 = new Class("__rOckingStar", 21, 123412412);
        Class user2 = new Class("xMoonSlayeerXx", 17, 124212);
        Class user3 = new Class("MenSlaughter", 12, 744212);

        System.out.println(user1.username + " " + user1.age + " " + user1.money);
        System.out.println(user2.username + " " + user2.age + " " + user2.money);
        System.out.println(user3.username + " " + user3.age + " " + user3.money);
    }
}