import java.lang.Math;
import java.math.BigInteger;

public class Calculator {
    private double number1;
    private double number2;

    public Calculator (double number1, double number2) {
        this.number1 = number1;
        this.number2 = number2;
    }

    public double add () {
        return (int) number1 + number2;
    }
    public double subtract () {
        return (int) number1 - number2;
    }
    public double multiply () {
        return (int) number1 * number2;
    }
    public double divide () {
        return (int) number1/number2;
    }
    public double mathpow () {
        return Math.pow(number1, number2);
    }
    public void factorial () {
        double num = number1;
        BigInteger factorial = BigInteger.ONE;
        for (int i = 1; i <= num; i++) {
            factorial = factorial.multiply(BigInteger.valueOf(i));
        }
        System.out.println("Factorial of " + number1 + " = " + factorial);
    }
    public double kokalti () {
        double radicand = number1;
        double degree = number2;
        double result = Math.pow(radicand, 1.0 / degree);
        System.out.println("The " + degree + "th root of " + radicand + " is " + result);
        return result;
    }
}
