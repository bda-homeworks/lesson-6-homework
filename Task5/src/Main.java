import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        char choice;
        do {
            Scanner input = new Scanner(System.in);
            System.out.print("Enter the first number: ");
            double number1 = input.nextDouble();
            System.out.print("Enter the second number: ");
            double number2 = input.nextDouble();

            System.out.println("Enter the operator: + - * / ^ !");
            char operation = input.next().charAt(0);

            Calculator calculator = new Calculator(number1, number2);

            switch (operation) {
                case '+':
                    System.out.println(calculator.add());
                    break;
                case '-':
                    System.out.println(calculator.subtract());
                    break;
                case '/':
                    System.out.println(calculator.divide());
                    break;
                case '*':
                    System.out.println(calculator.multiply());
                    break;
                case '^':
                    System.out.println(calculator.mathpow());
                    break;
                case '!':
                    System.out.println(calculator.kokalti());
                    break;
                default:
                    System.out.println("Invalid operation");
            }

            System.out.println("Do you want to continue? (y/n): ");
            choice = input.next().charAt(0);
        } while (choice == 'y');
    }
}