import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Employee s1 = new Employee();
        System.out.println("Tam adinizi qeyd edin:");
        s1.full_name = input.nextLine();
        System.out.println("Sobenizi qeyd edin:");
        s1.department = input.nextLine();
        System.out.println("Sirketinizi qeyd edin:");
        s1.company = input.nextLine();
        System.out.println("Vezifenizi qeyd edin:");
        s1.position = input.next();
        System.out.println("Maasinizi qeyd edin :)");
        s1.salary = input.nextInt();

        s1.printAllInfo();
    }
}