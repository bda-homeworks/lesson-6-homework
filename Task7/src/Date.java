public class Date {
    int day;
    int year;
    int month;

    Date () {
        this.day = 5;
        this.year = 2023;
        this.month = 5;
    }
    Date (int setDay, int setMonth, int setYear) {
        if (setDay > 31) {
            System.out.println("Ele bir gun yoxdur !!");
        } else {
            this.day = setDay;
        }
        if (setMonth > 12) {
            System.out.println("Ele bir ay yoxdur !!");
        } else {
            this.month = setMonth;
        }
        this.year = setYear;
    }

    public void setDay (int day) {
        this.day = day;
        System.out.println("Bugun ayin " + day);
    }
    public void setMonth (int month) {
        this.month = month;
        System.out.println("Bu ay: " + month);
    }
    public void setYear(int year) {
        this.year = year;
        System.out.println("Bu il:" + year);
    }
    String allDate () {
        return (this.day + "/" + this.month + "/" + this.year);
    }
}
