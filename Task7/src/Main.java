import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Qeyd etmek istediyiniz tarixi yazin: ");
        Date calendar = new Date(input.nextInt(), input.nextInt(), input.nextInt());
        System.out.println(calendar.allDate());

        char choice;
        do {
            Date calendar1 = new Date();
            System.out.println("Hansini deyisdirmek isteyirsiniz: (ay,gun,il)");
            String hesab = input.next();
            switch (hesab) {
                case "gun":
                    calendar.setDay(input.nextInt());
                    System.out.println(calendar.allDate());
                    System.out.println("Helede devam etmek isteyirsiz?");
                case "ay":
                    calendar.setMonth(input.nextInt());
                    System.out.println(calendar.allDate());
                    System.out.println("Helede devam etmek isteyirsiz?");
                case "il":
                    calendar.setYear(input.nextInt());
                    System.out.println(calendar.allDate());
                    System.out.println("Helede devam etmek isteyirsiz?");
            }
            choice = input.next().charAt(0);
        } while (choice == 'Y' || choice == 'y');
        System.out.println("Bye bye");
    }
}