public class Area {
    int length;
    int width;

    double getArea () {
        return length * width;
    }
    double getPerimeter () {
        return 2 * (length + width);
    }
}
