import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Area a1 = new Area();
        Scanner input = new Scanner(System.in);
        System.out.print("Enter the width of rectangle: ");
        a1.width = input.nextInt();
        System.out.print("Enter the length of rectangle: ");
        a1.length = input.nextInt();

        System.out.println("Area of rectangle: " + a1.getArea());
        System.out.println("Perimeter of rectangle: " + a1.getPerimeter());
    }
}