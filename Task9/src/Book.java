public class Book {
    String name;
    String price;
    String language;
    String author;
    String genre;
    String pages;

    public String name () {
        this.name = "Do Androids Dream of Electric Sheep?";
        return "Name of the book is: " + this.name;
    }
    public String price () {
        this.price = "35$ for a Hardcover";
        return "The price of the book is: " + this.price;
    }
    public String language () {
        this.language = "English";
        return "The language of the book is: " + this.language;
    }
    public String author () {
        this.author = "Philip K.Dick";
        return "The author of the book is: " + this.author;
    }
    public String genre () {
        this.genre = "Science fiction, philosophical fiction, noir fiction";
        return "The genre of the book is: " + this.genre;
    }
    public String pages () {
        this.pages = "210";
        return "Book consist of " + this.pages + " pages";
    }
    public String allinfo () {
        this.name = "Do Androids Dream of Electric Sheep?";
        this.price = "35$ for a Hardcover";
        this.language = "English";
        this.author = "Philip K.Dick";
        this.genre = "Science fiction, philosophical fiction, noir fiction";
        this.pages = "210";
        System.out.println("Name of the book is: " + this.name);
        System.out.println("The price of the book is: " + this.price);
        System.out.println("The language of the book is: " + this.language);
        System.out.println("The author of the book is: " + this.author);
        System.out.println("The genre of the book is: " + this.genre);
        return "Book consist of " + this.pages + " pages";
    }
}
