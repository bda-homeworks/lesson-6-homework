import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Book book = new Book();
        int choice;
        Scanner input = new Scanner(System.in);
        do {
            System.out.println("1. Kitabin adi barede melumat");
            System.out.println("2. Kitabin qiymeti barede melumat");
            System.out.println("3. Kitab hansi dildedir?");
            System.out.println("4. Kitabin yazari kimdir?");
            System.out.println("5. Kitab hansi janra aiddir?");
            System.out.println("6. Kitab nece sehifeden ibaretdir?");
            System.out.println("7. Butun melumatlari vermek.");
            choice = input.nextInt();
            switch (choice) {
                case 1:
                    System.out.println(book.name());
                    System.out.println("---------------------------------------------------------");
                    break;
                case 2:
                    System.out.println(book.price());
                    System.out.println("---------------------------------------------------------");
                    break;
                case 3:
                    System.out.println(book.language());
                    System.out.println("---------------------------------------------------------");
                    break;
                case 4:
                    System.out.println(book.author());
                    System.out.println("---------------------------------------------------------");
                    break;
                case 5:
                    System.out.println(book.genre());
                    System.out.println("---------------------------------------------------------");
                    break;
                case 6:
                    System.out.println(book.pages());
                    System.out.println("---------------------------------------------------------");
                    break;
                case 7:
                    System.out.println(book.allinfo());
                    System.out.println("---------------------------------------------------------");
                    break;
                default:
                    System.out.println("Invalid choice!");
                    break;
            }
        } while (choice != 7);
    }
}