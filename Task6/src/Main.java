import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int[] array = new int[input.nextInt()];
        for (int i = 0; i < array.length; i++) {
            array[i] = input.nextInt();
        }
        comparison(array);
        sort(array);
        indexfinder(array);
    }

    public static void comparison(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println("Array-iniz : " + array[i]);
        }
        Array a1 = new Array();
        System.out.println(a1.maxmin(array));
    }
    public static void sort(int[] array) {
        Array a1 = new Array();
        System.out.println(a1.sort(array));
    }
    public static void indexfinder(int[] array) {
        Scanner input = new Scanner(System.in);
        int index = input.nextInt();
        Array a1 = new Array();
        System.out.println("Element at the given index: " + index + " is " + array[index]);
        System.out.println(a1.indexfinder(array));
    }
}