import java.util.Arrays;
import java.util.Scanner;

public class Array {
    int [] array;

    public int maxmin(int[] array) {
        int[] numbers = array;

        int smallest = numbers[0];
        int largest = numbers[0];

        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] > largest) {
                largest = numbers[i];
            } else if (numbers[i] < smallest) {
                smallest = numbers[i];
            }
        }
        System.out.println("Largest number on array is " + largest);
        System.out.println("Smallest number on array is " + smallest);
        return 0;
    }
    public int sort(int[] array) {
        Arrays.sort(array);
        for (int i = 0; i < array.length; i++) {
            System.out.println("Sorted olunmus arrayiniz : " + array[i]);
        }
        return 0;
    }
    public int indexfinder(int[] array) {
        Scanner input = new Scanner(System.in);
        int index = input.nextInt();
        System.out.println("Element at the given index: " + index + " is " + array[index]);
        return 0;
    }
}
