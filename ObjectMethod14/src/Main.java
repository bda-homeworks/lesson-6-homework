import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        char choice;
        Scanner input = new Scanner(System.in);
        System.out.println("Birinci reqemi qeyd edin:");
        int number1 = input.nextInt();
        System.out.println("Ikinci reqemi qeyd edin:");
        int number2 = input.nextInt();
        System.out.println("Operatoru secin: + - / *");
        choice = input.next().charAt(0);
        switch (choice) {
            case '+':
                System.out.println("Cavab: " + toplama(number1, number2));
                break;
            case '-':
                System.out.println("Cavab: " + cixma(number1, number2));
                break;
            case '/':
                System.out.println("Cavab: " + bolme(number1, number2));
                break;
            case '*':
                System.out.println("Cavab: " + vurma(number1, number2));
                break;
        }
    }

    public static int toplama(int number1, int number2) {
        return number1 + number2;
    }

    public static int vurma(int number1, int number2) {
        return number1 + number2;
    }

    public static int bolme(int number1, int number2) {
        return number1 + number2;
    }

    public static int cixma(int number1, int number2) {
        return number1 + number2;
    }
}