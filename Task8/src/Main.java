import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Qeyd etmek istediyiniz tarixi yazin: ");
        Time calendar = new Time(input.nextInt(), input.nextInt(), input.nextInt());
        System.out.println(calendar.allDate());

        char choice;
        do {
            Time calendar1 = new Time();
            System.out.println("Hansini deyisdirmek isteyirsiniz: (ay,gun,il)");
            String hesab = input.next();
            switch (hesab) {
                case "gun":
                    calendar.setSecond(input.nextInt());
                    System.out.println(calendar.allDate());
                    System.out.println("Helede devam etmek isteyirsiz?");
                case "ay":
                    calendar.setMinute(input.nextInt());
                    System.out.println(calendar.allDate());
                    System.out.println("Helede devam etmek isteyirsiz?");
                case "il":
                    calendar.setHour(input.nextInt());
                    System.out.println(calendar.allDate());
                    System.out.println("Helede devam etmek isteyirsiz?");
            }
            choice = input.next().charAt(0);
        } while (choice == 'Y' || choice == 'y');
        System.out.println("Bye bye");
    }
}