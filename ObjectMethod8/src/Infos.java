public class Infos {
    String name;
    int age;
    String surname;

    public Infos(String name, int age, String surname) {
        this.name = name;
        this.age = age;
        this.surname = surname;
    }
}
