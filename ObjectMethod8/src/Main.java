public class Main {
    public static void main(String[] args) {
        Infos user1 = new Infos("Bayram", 21, "Nurlu");
        Infos user2 = new Infos("Faiq", 45, "Agayev");
        Infos user3 = new Infos("Elikram", 17, "Memmedoglu");

        System.out.println(user1.age + " " + user1.name + " " + user1.surname);
        System.out.println(user2.age + " " + user2.name + " " + user2.surname);
        System.out.println(user3.age + " " + user3.name + " " + user3.surname);
    }
}